from django.db import models

# Create your models here.
class Student(models.Model):
   student_name = models.CharField(max_length=100)
   student_email = models.CharField(max_length=50)
   section = models.CharField(max_length=50)
   subjects = models.CharField(max_length=500)
   gender = models.CharField(max_length=50)
   dob = models.DateField(auto_now=False, auto_now_add=False)